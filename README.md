# RedditNow

Really simple script to submit a self post to Reddit. It can be used with crontab to schedule a post, for example.

## Setup

Put `praw.ini` in `~/.config`. The contents should be the following:

```
[bot username]
username=bot username
password=bot password
client_id=bot client ID
client_secret=bot client secret
```

For example:

```
[GallowBoob]
username=GallowBoob
password=hunter2
client_id=crgythuyi3o
client_secret=hbytuniljokihugys6fd
```

For more, see [PRAW's page on the praw.ini
file](https://praw.readthedocs.io/en/latest/getting_started/configuration/prawini.html).

## Usage

```commandline
python3 redditnow/main.py [username] [subreddit] [path to post]
```

The username passed should match the username in the praw.ini described above. The subreddit should be one in which your
account has permission to make self posts. The path to the post should be an (ideally absolute) path to a text file.

The text file describing the post is as follows:

```text
My awesome title![newline]
The body of the post,
which might be multiple lines. In **Reddit Markdown**.
```
