import praw


class Poster:
    @property
    def reddit(self):
        if self._reddit is None:
            self._reddit = praw.Reddit(
                self.acct_name, user_agent="redditnow by gitlab/jarhill0"
            )
        return self._reddit

    def __init__(self, acct_name):
        self.acct_name = acct_name
        self._reddit = None

    def make_self_post(self, sr_name, title, post_text):
        subreddit = self.reddit.subreddit(sr_name)
        subreddit.submit(title=title, selftext=post_text)
