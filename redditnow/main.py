from sys import argv
from get_post_contents import get_contents
from poster import Poster


def main():
    assert (
        len(argv) == 4
    ), "Must provide exactly and only [account name] [subreddit name] [path to post]"
    _, acct_name, sr_name, post_path = argv
    poster = Poster(acct_name=acct_name)
    contents = get_contents(post_path)
    title, _, body = contents.partition("\n")
    poster.make_self_post(sr_name, title, body)


if __name__ == "__main__":
    main()
