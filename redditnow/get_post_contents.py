def get_contents(filename):
    try:
        with open(filename) as f:
            return f.read()
    except OSError:
        return "Sorry, the contents of this post couldn't be found."
